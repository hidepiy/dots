#!/bin/bash
DIR=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)
FILES=(tmux.conf zshrc vimrc vim)

echo "git submodule init"
git submodule init
echo "git submodule update"
git submodule update

for file in ${FILES[@]}; do
  if test -e ~/.${file}; then
    echo "~/.${file} already exists!!!"
  else
    ln -sv ${DIR}/${file} ~/.${file}
  fi
done

