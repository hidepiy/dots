""""""""""
" ENV
""""""""""
set clipboard=unnamed,unnamedplus
set mouse=a
set ttymouse=xterm2
set shellslash "set path delimiter as / even Windows
"set iminsert=2 "automatically turn off IME while leaving insert mode
set wildmenu wildmode=list:longest,full "enable file name completion with TAB
set history=10000 "num of command line history
set visualbell t_vb= "disable beep sound
set noerrorbells "disable beep sound for error message

""""""""""
" CURSOR
""""""""""
set backspace=indent,eol,start
set whichwrap=b,s,h,l,<,>,[,]
set scrolloff=8
set sidescrolloff=16
set sidescroll=1

""""""""""
" FILE
""""""""""
set confirm
set hidden
set autoread
set nobackup
set noswapfile

""""""""""
" FIND
""""""""""
set hlsearch
set incsearch
set ignorecase
set smartcase
set wrapscan
set gdefault

""""""""""
" VIEW
""""""""""
syntax on
"set termguicolors
"colorscheme gotham
set bg=dark
if v:version >= 800
  colorscheme gruvbox
endif
filetype plugin indent on
set number
set title
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]
set display=lastline
set cursorline
set cursorcolumn
set laststatus=2
set cmdheight=1
set pumheight=10
set showmatch
set showcmd
set matchtime=1
set showmode
set helpheight=999
set ruler
set list
set listchars=tab:▸\ ,eol:↲,extends:❯,precedes:❮
highlight link ZenkakuSpace Error
match ZenkakuSpace /　/

""""""""""
" TAB INDENT
""""""""""
set expandtab
set tabstop=2 shiftwidth=2 softtabstop=0
set autoindent
set smartindent
set smarttab

""""""""""
" BIND
""""""""""
noremap Y y$
noremap j gj
nnoremap k gk
nnoremap <silent><C-e> :NERDTreeToggle<CR>
set whichwrap=b,s,h,l,<,>,[,]
nnoremap + <C-a>
nnoremap - <C-x>
imap <c-o> <END>
imap <c-a> <HOME>
imap <c-h> <LEFT>
imap <c-j> <DOWN>
imap <c-k> <UP>
imap <c-l> <Right>

