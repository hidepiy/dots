##########
# ENV
##########
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export EDITOR=vim

bindkey -e

# start Tetris with Ctrl-t
autoload -Uz tetris
zle -N tetris
bindkey '^T' tetris

##########
# HISTORY
##########
HISTFILE=~/.zsh_history
HISTSIZE=5000
SAVEHIST=5000

setopt extended_history
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_no_store
setopt hist_reduce_blanks
setopt hist_verify
setopt inc_append_history
setopt share_history
setopt interactivecomments

autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line

##########
# FUNCTION
##########
function reload() {
  source ~/.zshrc
  echo "execute : source ~/.zshrc"
}

function pathtowin() {
  echo -E "${1}" | sed -E 's/smb:\/\//\\\\/' | sed -E 's/\//\\/g' | tr -d '\n'
}

function pathtomac() {
  echo -E "${1}" | sed -E 's/\\+/smb:\/\//' | sed -E 's/\\/\//g' | tr -d '\n'
}

##########
# ALIAS
##########
alias view="vim -R"
alias gistory='git log --pretty=format:"%h %cd %cn %s" --graph | head'

##########
# COMP
##########
autoload -U compinit
compinit
setopt auto_cd
setopt auto_list
setopt auto_menu
setopt auto_param_keys
setopt auto_param_slash
setopt auto_pushd
setopt correct
setopt correct_all
setopt hist_expand
setopt list_packed
setopt list_types
setopt mark_dirs
setopt nolistbeep
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

##########
# COLOR
##########
#export TERM=xterm-256color
autoload -Uz colors
colors
export LSCOLORS=gxfxcxdxbxegedabagacad
zstyle ':completion:*' list-colors 'di=36;49'

##########
# PROMPT
##########
setopt prompt_subst
PROMPT="%D{%Y-%m-%d %H:%M:%S} %{${fg[cyan]}%}%n@%m %{${fg[yellow]}%}%d
%#%{${reset_color}%} "

PROMPT2="%{${fg[yellow]}%}%_%%%{${reset_color}%} "
SPROMPT="%{${fg[red]}%}%r is correct? [n,y,a,e]:%{${reset_color}%} "
RPROMPT=""
# reset prompt before executing command for showing command execution time
re-prompt() {
    zle .reset-prompt
    zle .accept-line
}
zle -N accept-line re-prompt

##########
# VCS
##########
autoload -Uz vcs_info
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' stagedstr "%F{yellow}!"
zstyle ':vcs_info:git:*' unstagedstr "%F{red}+"
zstyle ':vcs_info:*' formats "%F{green}%c%u[(%s) %r:%b]%f"
zstyle ':vcs_info:*' actionformats '[(%s) %r:%b|%a]'
precmd () { vcs_info }
RPROMPT='${vcs_info_msg_0_}'$RPROMPT


[ -f ~/.zshrc.include ] && source ~/.zshrc.include
export PATH

